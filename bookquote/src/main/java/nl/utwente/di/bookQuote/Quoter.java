package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {
    public double getBookPrice(String isbn) {
        HashMap<String, Double> bookIsbn = new HashMap<String, Double>();

        bookIsbn.put("1", 10.0);
        bookIsbn.put("2", 45.0);
        bookIsbn.put("3", 20.0);
        bookIsbn.put("4", 35.0);
        bookIsbn.put("5", 50.00);

        for (String val : bookIsbn.keySet()) {
            if (val.equals(isbn)) {
                return bookIsbn.get(val);
            }
        }
        return 0.0;
    }
}